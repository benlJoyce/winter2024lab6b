public class gameManager{
	private Deck drawPile;
	private Card centerCard;
	private Card playerCard;
	
	public gameManager(){
		drawPile = new Deck();
		drawPile.shuffle();
		centerCard = drawPile.drawTopCard();
		playerCard = drawPile.drawTopCard();
	}
	@Override
	public String toString(){
		return "\n-------------------------------------------------------------" + 
		"\n Center card: " + centerCard.getValue() + " of " + centerCard.getSuit() +
		"\n Player card: " + playerCard.getValue() + " of " + playerCard.getSuit() +
		"\n-------------------------------------------------------------";
	}
	public void dealCards(){
		drawPile.shuffle();
		centerCard = drawPile.drawTopCard();
		playerCard = drawPile.drawTopCard();
	}
	public int getNumberOfCards(){
		return drawPile.length();
	}
	public int calculatePoints(){
		if(centerCard.getValue().equals(playerCard.getValue())){
			return 4;
		}else if(centerCard.getSuit().equals(playerCard.getSuit())){
			return 2;
		} else {
			return -1;
			
		}
	}
}