public class LuckyCardGameApp{
	public static void main(String[] args) {
		gameManager manager = new gameManager();
		int totalPoints = 0;
	
		System.out.println("Welcome to Lucky! Card Game!");
		
		int round = 0;
		while(manager.getNumberOfCards() > 1 &&  totalPoints  < 5){
			System.out.println("Round: " + round);
			System.out.println(manager.toString());
			totalPoints += manager.calculatePoints();
			System.out.println("Your points after round: " + round + " is: " + totalPoints);
			System.out.println("******************************************************************");
			manager.dealCards();
			round++;
		}
		if(totalPoints >= 5){
			System.out.println("Player wins with: " + totalPoints + " points");
		} else{
			System.out.println("Player loses with: " + totalPoints + " points");
		}
	}
}