import java.util.Random;
public class Deck{
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	public Deck(){
		rng = new Random();
		cards = new Card[52];
		
		String[] suits = {"Hearts", "Diamonds", "Clubs", "Spades"};
		String[] values = {"Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"};
		
		int index = 0;
		for(String suit : suits){
			for(String value : values){
				cards[index] = new Card(suit, value);
				index++;
			}
		}
		numberOfCards = cards.length;

	}
	public int length(){
		return numberOfCards;
	}
	public Card drawTopCard(){
		if(numberOfCards > 0){
			numberOfCards--;
			return cards[numberOfCards];
		} else {
			return null; 
		}
	}
	public String toString(){
		if(numberOfCards == 0){
			return "Deck is empty";
		}	
		String deckString = "";
		for(int i = 0; i < numberOfCards; i++){
			deckString += this.cards[i].toString() + "\n";
		}
		return deckString;
	}
	public void shuffle(){
		for(int i = 0; i < this.cards.length; i ++){
			//shuffles current card to a random card only considering unshuffled cards
			int randomIndex = rng.nextInt(cards.length - i) + i;
			
			Card currentCard = this.cards[i];
			this.cards[i] = this.cards[randomIndex];
			this.cards[randomIndex] = currentCard;
		}
	}
}

